---
version: 2
type de projet: Projet de semestre
année scolaire: 2021/2022
titre: SMI Protocol Analyzer using Saleae
abréviation: SMIAnalyzer
filières:
  - Télécommunications
  - Informatique
mandants:
  - Sonova SA (Phonak SA)
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Nicolas Schroeter
mots-clés: [Protocol Analyzer, Wireless Communication, Saleae, XML]
langue: [F,E,D]
confidentialité: oui
réalisation: labo
suite: non
nombre d'étudiants: 1
---
## Description/Context

In the scope of the development of an ASIC for wireless communication. Sonova (Phonak Communications) has developed a custom serial communication protocol to transport half-duplex control and audio data on 2 wires (Sonova IP). 
To debug SW and HW developers are using Saleae Logic analyzer ( [https://www.saleae.com/](https://www.saleae.com) ). Saleae propose an API to implement a custom protocol analyzer that can be used for customization.

## Contraints

* Use developpement process and tools recommanded by Sonova and running on Microsoft OS

* Link to Saleae [SDK Protocol Analyzer SDK - Saleae Support](https://support.saleae.com/saleae-api-and-sdk/protocol-analyzer-sdk). The physical layer is fully described in an internal Sonova document

* Use XML for Application layer primitives description (already defined)

* Running on Windows 10.

## Objectives/Tasks

* Analyze and describe in details the interface that allow to interact with the Saleae protocol analyzer

* Define a well structured architecture that will be able decode the Sonova communication protocol

* Design, develop and validate a plug-in for the Saleae SW that decodes the physical layer of the Sonova protocol on Windows 10 which includes synchro, siganling, control and audio data

* Design, develop and validate the application layer protocol decoder
