---
version: 2
type de projet: Projet de semestre
année scolaire: 2021/2022
titre: Aligneur de bouchons
abréviation: PosBouchon
filières:
  - Télécommunications
mandants:
  - Polytype SA
nombre d'étudiants: 1
mots-clés: [MCA, Système embarqué, Image Processing, Commande]
langue: [F,E,D]
confidentialité: non
réalisation: entreprise
suite: non
nombre d'étudiants: 1
attribué à:
  - Nicolas Wirth
---
## Description/Context
L'entreprise Polytype développe, construit et installe des machines capables d'imprimer des motifs de couleur sur des tubes de plastique de taille et de formes variées. Dans le cadre du programme MCA (Motion Control Academie), l'entreprise Polytype demande de développer une machine complète capable d'aligner et de positionner des bouchons de manière à pouvoir ensuite les sertir correctement sur les tubes.
Il s'agit de développer, en équipe avec un étudiant GM et un étudiant GE qui seront en charge des aspects mécanique resp. électronique du système, la partie informatique (IT) du projet " Aligneur de bouchon ".
Le système sera capable de détecter sans contact (analyse optique, image ou autre...) l'orientation d'un bouchon flip-top (de tube ou de bouteille ) alimenté d'un tapis roulant où il est posé debout, ceci sur la base de sa forme légèrement non circulaire ou présentant une particularité (p. ex clapet) qui en définit l'orientation. Sur la base de cette information, il s'agira alors de réorienter le bouchon sur le tapis roulant. 
L'étudiant sera en charge de la partie traitement numérique du projet. Pour ce faire, il s'agira de choisir une plateforme, définir le ou les algorithmes de traitement, les programmer, et mettre en oeuvre les liaisons avec les systèmes de handling et d'acquisition.

## Contraintes

* Contraintes organisationnelles induites par la conduite du programme MCA, incluant la définition interdisciplinaire, la gestion globale du projet et la coordination avec le mandant.
* Utilisation et mise en oeuvre des différents processus de développement recommandés par Polytype SA
* Utilisation d'un système de traitement minimisant les ressources (calcul, mémoire, énergie, etc.) et compatible avec les intentions du mandant.

## Objectifs/Tâches

* Analyse des fonctionnalités globales du système en collaboration étroite avec les étudiants des autres filières (métiers)
* Définition de l'architecture globale du système en collaboration étroite avec les étudiants des autres filières (métiers) 
* Définition fonctionnelle (spécification) du système de traitement numérique
* Elaboration de l'architecture IT du système et validation de cette dernière par la réalisation d'un premier prototypage.
